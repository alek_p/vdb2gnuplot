#### Depreciated in favor of https://bitbucket.org/alek_p/vdb2pyplot/src




Script to help with graphing vdbench output.
It assumes that elapsed= and interval= values are not changed in the vdbench config file between runs in the same flatfile.
Dependencies are vdbench and gnuplot.

vdb2gnuplot v1.4 Usage:

   ./vdb2gnuplot.sh -f path/to/flatfile.html -n #.of_samples_in_run [-i|-b|-l|-c|-a] [-R #,#,#...] [-p str.data_displ] [-r #.plot_this_rdpct] [-t #.plot_this_threads] [-s #.bytes_plot_this_xfersize] [-k]
      -f path to flatflie.html (required)
      -n samples per run ( elapsed / interval ) (required)
      -i plot IOPS graph (default)
      -b plot MB/s graph
      -l plot latency (ms) graph
      -c plot cpu utilization (usr+sys) % graph
      -p gnuplot data display format (default is linespoints)
      -R plot only specified runs
      -r only plot runs that have specified "rdpct" used (supported for Storage definitions only)
      -t only plot runs that have specified "threads" used (supported for Storage definitions only)
      -s only plot runs that have specified "xfersize" used
      -k put the graph key above title
      -a plot all graphs (-l -i -b -c)

Download and Run:
# wget --no-check-certificate https://bitbucket.org/alek_p/vdb2gnuplot/raw/HEAD/vdb2gnuplot.sh
# chmod +x vdb2gnuplot.sh

To use the script you need the location of the flatfile.html and the number of samples per run. To get the samples per run number divide the value of vdbench conf 'elapsed' by 'interval'.
# ./vdb2gnuplot -f output/flatfile.html -n (elapsed/interval)